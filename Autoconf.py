# coding=utf-8

from scapy.all import *
from scapy.layers.inet import IP
from scapy.layers.l2 import Ether


class Autoconf:

    def __init__(self):
        self.host_mac = ""
        self.host_ip = ""
        self.conf = True
        self.ifaceHost = "em1"
        self.ifaceNetwork = "eth0"
        self.sockHost = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(0x0003))
        self.sockNetwork = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(0x0003))
        try:
            self.sockHost.bind((self.ifaceHost, 0))
            self.sockNetwork.bind((self.ifaceNetwork, 0))
        except e:
            # exit("You need 2 physical network interfaces to use FENRIR !")
            print("You need 2 physical network interfaces to use FENRIR !")
        self.inputs = [self.sockHost, self.sockNetwork]

    def start_autoconf(self):
        print("Trying to detect @mac and @ip of spoofed host...")
        while self.conf:
            try:
                input_ready, output_ready, except_ready = select(self.inputs, [], [])
            except e:
                break
            for socketReady in input_ready:
                # We check packets from iface1 and fwd them to iface2
                if socketReady == self.sockHost:
                    raw_packet = self.sockHost.recvfrom(1500)
                    pkt = raw_packet[0]
                    dpkt = Ether(raw_packet[0])
                    if 'ARP' in dpkt:
                        self.host_mac = dpkt[Ether].src
                    elif 'IP' in dpkt:
                        self.host_ip = dpkt[IP].src
                        self.host_mac = dpkt[Ether].src
                    # We send the packet to the other interface
                    self.sockNetwork.send(pkt)
                # We forward packet from iface2 to iface1
                if socketReady == self.sockNetwork:
                    raw_packet = self.sockNetwork.recvfrom(1500)
                    pkt = raw_packet[0]
                    self.sockHost.send(pkt)
            if self.host_ip != "" and self.host_mac != "":
                # self.conf = False
                return self.host_ip, self.host_mac
