# coding=utf-8
######################################################################
##|# ------ FenrirTail : Fenrir logging and output class ----- #|###
##|# -    It is responsible for printing error/debug/info    - #|###
##|# -                 messages to the user                  - #|###
######################################################################

from scapy.all import *
from sys import stdout

###########################################################################
### ---------------- Main component of FenrirTail ---------------- ####
###########################################################################
from scapy.layers.inet import IP
from scapy.layers.l2 import Ether


## fenrirPanic : unrecoverable exception handling ##
def fenrir_panic(msg, bold=1, exit_on_failure=1):
    if bold == 1:
        msg = '\033[1m' + 'FENRIR PANIC : ' + msg + '\033[0m'
    else:
        msg = 'FENRIR PANIC : ' + msg
    if exit_on_failure == 1:
        exit(msg)
    else:
        print(msg)


class FenrirTail:

    def __init__(self, debug_level=1):
        self.debug = debug_level
        self.threshold = 100

    @staticmethod
    def packet_counter(pkt, pkt_number, pkt_thread_number):
        stdout.write("\r\033[1m\033[32m[RAWR]\033[0m Processing packet number\033[1m \033[31m" + str(
            pkt_number))
        stdout.flush()

    # Verbosity : 0 = no msg, 1 = normal, 2 = information (light), 3 = this damn tool won't stop printing stuff
    ## Notify : main function for standard output ##
    def notify(self, msg, verbosity_level, bold=0):
        if self.debug >= verbosity_level:
            if bold == 1:
                msg = '\033[1m' + msg + '\033[0m'
            else:
                msg = '[-- ' + msg
            print(msg)

    ## notifyGood : green color ##
    def notify_good(self, msg, verbosity_level, bold=0):
        if self.debug >= verbosity_level:
            msg = '\033[32m[*] ' + msg + '\033[0m'
            self.notify(msg, verbosity_level, bold)

    ## notifyWarn : yellow color ##
    def notify_warn(self, msg, verbosity_level, bold=0):
        if self.debug >= verbosity_level:
            msg = '\033[33m[*] ' + msg + '\033[0m'
            self.notify(msg, verbosity_level, bold)

    ## notifyBad : red color ##
    def notify_bad(self, msg, verbosity_level, bold=0):
        if self.debug >= verbosity_level:
            msg = '\033[31m[*] ' + msg + '\033[0m'
            self.notify(msg, verbosity_level, bold)

    ## mangleException : responsible for writing mangle exceptions logs to file ##
    def mangle_exception(self, pkt, reason=''):
        self.notify_bad('\nFENRIR PANIC : Process failed during MANGLING', 1, 1)
        if reason != '':
            self.notify_bad('Reason : ' + reason, 1)
        self.notify('Packet was logged to errorLogFile : FENRIR.err', 1)
        logfd = open('FENRIR.err', 'a')
        logfd.write(
            '---DUMP BEGINS--------------------------------------------------------------------------------------\n')
        logfd.write(
            '[*] Packet header SRC : ' + pkt[IP].src + ' (' + pkt[Ether].src + ') DST : ' + pkt[IP].dst + ' (' + pkt[
                Ether].dst + ')\n')
        logfd.write('Packet dump :\n')
        logfd.write(str(ls(pkt)) + '\n')
        logfd.write(
            '---DUMP ENDS----------------------------------------------------------------------------------------\n')
        logfd.close()


