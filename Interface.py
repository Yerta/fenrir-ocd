# coding=utf-8

from cmd2 import Cmd
from binascii import unhexlify
from FENRIR2 import *
import threading


class Interface(Cmd):
    FENRIR = FENRIR()
    FenrirThread = None
    stop_event = None
    promptBase = "FENRIR"
    prompt = "\n\033[1m\033[31m" + promptBase + " >\033[0m "
    intro = """\n\033[1m
	                                                  ,a8b
	                                              ,,od8  8
	                                             d8'     8b
	                                          d8'ba     aP'
	                                       o8'         aP'
	                                        YaaaP'    ba
	                       \033[31mFENRIR\033[0m\033[1m         Y8'         88
	                                   ,8\"           `P
	                              ,d8P'              ba
	              ooood8888888P\"\"\"'                  P'
	           ,od                                  8
	        ,dP     o88o                           o'
	       ,dP          8                          8
	      ,d'   oo       8                       ,8
	      $    d$\"8      8           Y    Y  o   8
	     d    d  d8    od  \"\"boooaaaaoob   d\"\"8  8
	     $    8  d  ood'-I   8         b  8   '8  b
	     $   $  8  8     d  d8        `b  d    '8  b
	      $  $ 8   b    Y  d8          8 ,P     '8  b
	      `$$  Yb  b     8b 8b         8 8,      '8  o,
	           `Y  b      8o  $$       d  b        b   $o
	            8   '$     8$,,$\"      $   $o      '$o$$
	            $o$$P\"                 $$o$
	\033[0m"""

    def __init__(self):
        Cmd.__init__(self)

    ### TOOLBOX ###
    @staticmethod
    def hex_to_str(hex_str):
        string = hexlify(hex_str).decode('ascii')
        return string[:2] + ":" + string[2:4] + ":" + string[4:6] + ":" + string[6:8] + ":" + string[
                                                                                              8:10] + ":" + string[-2:]

    @staticmethod
    def str_to_hex(string):
        hexes = string.split(":")
        hex_str = ''.join(hexes).encode("ascii")
        return unhexlify(hex_str)

    def change_running_state(self, state):
        if state:
            self.prompt = "\n\033[1m\033[32m" + self.promptBase + " >\033[0m "
            self.FENRIR.isRunning = True
        elif not state:
            self.prompt = "\n\033[1m\033[31m" + self.promptBase + " >\033[0m "
            self.FENRIR.isRunning = False

    def do_create_virtual_tap(self, s):
        self.FENRIR.create_tap()

    @staticmethod
    def help_create_virtual_tap():
        print("Creates the virtual tap for FENRIR core module")

    def do_destroy_virtual_tap(self, s):
        self.FENRIR.down_tap()

    @staticmethod
    def help_delete_virtual_tap():
        print("Deletes the virtual tap for FENRIR core module")

    def do_show(self, arg_string):
        argv = arg_string.split()
        if len(argv) != 1:
            print("*** Invalid number of arguments")
            self.help_show()
        else:
            if argv[0] == "tap" and self.FENRIR.tap is not None:
                print("tap :")
                print("Address ===> " + self.FENRIR.tap.addr)
                print("MAC ===> " + self.hex_to_str(self.FENRIR.tap.hwaddr))
                print("mtu ===> " + str(self.FENRIR.tap.mtu))
            elif argv[0] == "host_ip" and self.FENRIR.host_ip is not None:
                print("host_ip ===> " + self.FENRIR.host_ip)
            elif argv[0] == "host_mac" and self.FENRIR.host_mac is not None:
                print("host_mac ===> " + self.hex_to_str(self.FENRIR.host_mac))
            elif argv[0] == "rules":
                if self.FENRIR.FenrirFangs.ruleCount == 0:
                    print("No rule added (yet)")
                else:
                    num = 0
                    for rule in self.FENRIR.FenrirFangs.user_rules:
                        num += 1
                        print("Rule " + str(num) + " : \n\tport = " + str(
                            rule.dst_port) + "\n\ttype = " + rule.type + "\n\tproto = " + rule.proto)
            elif argv[0] == "netIface":
                print("netIface ===> " + self.FENRIR.switch_iface)
            elif argv[0] == "hostIface":
                print("hostIface ===> " + self.FENRIR.lhost_iface)
            elif argv[0] == "all":
                self.do_show("tap")
                self.do_show("host_ip")
                self.do_show("host_mac")
                self.do_show("hostIface")
                self.do_show("netIface")
                self.do_show("rules")

    @staticmethod
    def help_show():
        print("USAGE : show <attribute>")

    @staticmethod
    def complete_show(match, line, bindex, eindex):
        COMPLETION_ARRAY = ('tap', 'host_ip', 'host_mac', 'rules', 'hostIface ', 'netIface ', 'all')
        return [i for i in COMPLETION_ARRAY if i.startswith(match)]

    def do_set(self, arg_string):
        argv = arg_string.split()
        if len(argv) != 2:
            print("*** Invalid number of arguments")
            self.help_set()
        else:
            attr_value = None
            if argv[0] == "debug":
                Cmd.do_set(self, arg_string)
            elif argv[0] == "host_mac":
                attr_value = self.str_to_hex(argv[1])
            else:
                attr_value = argv[1]
            if not self.FENRIR.set_attribute(argv[0], attr_value):
                print("*** Invalid argument")
                self.help_set()
            else:
                print(argv[0] + " ===> " + argv[1])

    @staticmethod
    def help_set():
        print("USAGE : set <attribute> <value>")
        print("Attributes = host_ip, host_mac, netIface, hostIface, verbosity <0-3>")

    @staticmethod
    def complete_set(match, line, bindex, eindex):
        COMPLETION_ARRAY = ('host_ip ', 'host_mac ', 'verbosity ', 'netIface ', 'hostIface ')
        if bindex == 4:
            return [i for i in COMPLETION_ARRAY if i.startswith(match)]
        else:
            return ''

    def do_stats(self, s):
        if self.FENRIR.isRunning:
            print("Packet(s) processed by FENRIR : " + str(self.FENRIR.pktsCount))

    def do_add_reverse_rule(self, arg_string):
        argv = arg_string.split()
        if len(argv) != 3:
            print("*** Invalid number of arguments")
            self.help_add_reverse_rule()
        else:
            try:
                argv[0] = int(argv[0])
            except e:
                print("*** First agument must be a number")
                self.help_add_reverse_rule()
            TYPES_ARRAY = ('unique', 'multi')
            if 65535 >= argv[0] > 0 and argv[1] in TYPES_ARRAY:
                self.FENRIR.FenrirFangs.add_rule(argv[0], argv[2], argv[1])
                print(
                    "New rule added : \n\tport = " + str(argv[0]) + "\n\ttype = " + argv[1] + "\n\tproto = " + argv[2])
            else:
                print("*** Invalid arguments")
                self.help_add_reverse_rule()

    @staticmethod
    def help_add_reverse_rule():
        print("USAGE : add_reverse_rule <port> <type = unique> <proto = IP>")
        print(
            "Interface for adding port-specific rules to allow reverse connection to reach FENRIR. This is usefull for reverse shell or for server-based exploits & fun (Responder)")
        print(
            "Types include : \n\tunique = rule is triggered once before being deleted (usefull to get a reverse shell from one host) \n\tmulti = rule can be triggered multiple times (usefull for MitM stuff)")

    @staticmethod
    def complete_add_reverse_rule(match, line, bindex, eindex):
        if bindex <= 16:
            return ' '
        elif bindex > 16:
            if line.count(' ') == 2:
                COMPLETION_ARRAY = ('unique ', 'multi ')
                return [i for i in COMPLETION_ARRAY if i.startswith(match)]
            elif line.count(' ') >= 3:
                return ''
            else:
                return ''
        else:
            return ''

    def do_autoconf(self, s):
        print("Running initAutoconf...")
        self.FENRIR.init_autoconf()
        self.do_show('all')

    @staticmethod
    def help_autoconf():
        print("Runs the auto-configuration module")

    def do_run(self, s):
        if self.FENRIR.tap is None:
            self.do_create_virtual_tap("")
        if self.FENRIR.tap is not None and self.FENRIR.host_ip != '' and self.FENRIR.host_mac != '':
            self.FENRIR.set_attribute("verbosity", 0)
            self.change_running_state(True)
            self.stop_event = threading.Event()
            self.FenrirThread = threading.Thread(target=self.FENRIR.init_mangle, args=(self.stop_event,))
            self.FenrirThread.daemon = True
            self.FenrirThread.start()
        #			self.FENRIR.initMANGLE()
        else:
            print("*** FENRIR PANIC : Configuration problem")
            self.help_run()

    @staticmethod
    def help_run():
        print("USAGE : run")
        print("This will launch FENRIR core in a new thread and remove any verbosity !")
        print(
            "(Disclaimer : you must have run the auto-configuration module or given correct information manually before running this command ! You need at least host_ip, host_mac and a virtual tap created !)")

    def do_run_debug(self, s):
        if self.FENRIR.tap is None:
            self.do_create_virtual_tap("")
        if self.FENRIR.tap is not None and self.FENRIR.host_ip != '' and self.FENRIR.host_mac != '':
            self.change_running_state(True)
            self.stop_event = threading.Event()
            self.FENRIR.init_mangle(self.stop_event)
        else:
            print("*** FENRIR PANIC : Configuration problem")
            self.help_run_debug()

    @staticmethod
    def help_run_debug():
        print("USAGE : run_debug")
        print("This will launch FENRIR core WITHOUT creating a new thread !")
        print(
            "(Disclaimer : you must have run the auto-configuration module or given correct information manually before running this command ! You need at least host_ip, host_mac and a virtual tap created !)")

    def do_stop(self, s):
        if self.FENRIR.isRunning:
            self.stop_event.set()
            self.FenrirThread.join()
            self.change_running_state(False)
            print("Fenrir was stopped")
        else:
            print("Fenrir is not running at the moment...")

    @staticmethod
    def help_stop():
        print("Stops the FENRIR thread")

    @staticmethod
    def do_cookie(s):
        print("This cookie machine is brought to you by Valérian LEGRAND valerian.legrand@orange.com\n")
        print("COOKIE COOKIE COOKIE")
        print("COOKIE COOKIE COOKIE")
        print("COOKIE COOKIE COOKIE")
        print("COOKIE COOKIE COOKIE")
        print("COOKIE COOKIE COOKIE")

    @staticmethod
    def do_exit(s):
        return True

    def do_help(self, s):
        if s == '':
            print("FENRIR Commands :")
            print("\tcookie")
            print("\tcreate_virtual_tap")
            print("\tdestroy_virtual_tap")
            print("\tadd_reverse_rule")
            print("\trun")
            print("\trun_debug")
            print("\tset")
            print("\tshell")
            print("\tshortcuts")
            print("\tautoconf")
            print("\tstop")
            print("\tquit")
            print("\thelp")
        else:
            Cmd.do_help(self, s)

    def complete_help(self, match, line, bindex, eindex):
        COMPLETION_ARRAY = (
            'cookie', 'create_virtual_tap', 'destroy_virtual_tap', 'add_reverse_rule', 'run', 'run_debug', 'set',
            'shell',
            'shortcuts', 'autoconf', 'stop', 'quit', 'exit', 'help')
        return [i for i in COMPLETION_ARRAY if i.startswith(match)]


if __name__ == '__main__':
    app = Interface()
    app.cmdloop()
