# coding=utf-8

from scapy.all import *
from FenrirTail import FenrirTail


class FenrirFangs:

    def __init__(self, debug_level=1):
        self.debugLevel = debug_level
        self.FenrirTail = FenrirTail(debug_level)
        self.user_rules = []
        self.ruleCount = 0

    # self.addRule(137, 'IP', 'multi')
    # self.addRule(5355, 'IP', 'multi')
    # self.addRule(80,'IP', 'multi')
    # self.addRule(445,'IP','unique')

    def add_rule(self, port_dst, proto='IP', rule_type='unique'):
        user_rule = FenrirRule(port_dst, proto, rule_type)
        self.user_rules.append(user_rule)
        self.ruleCount = self.ruleCount + 1

    def check_rules(self, pkt):
        for rule in self.user_rules:
            if rule.pkt_match(pkt):
                if rule.type == 'unique':
                    self.user_rules.remove(rule)
                    self.ruleCount = self.ruleCount - 1
                return True
        # if 'IP' in pkt and pkt[IP].src == '10.0.0.69':
        #	print('DEDANS')
        #	return True
        return False

    def change_verbosity(self, debug_level):
        self.debugLevel = debug_level


class FenrirRule:

    def __init__(self, port_dst, proto='IP', rule_type='unique'):
        self.dst_port = port_dst
        self.proto = proto
        self.type = rule_type

    def pkt_match(self, pkt):
        epkt = pkt
        try:
            if self.proto in epkt:
                if 'IP' in epkt and epkt['IP'].dport == self.dst_port:
                    return True
                else:
                    if epkt[self.proto].dport == self.dst_port:
                        return True
                    else:
                        return False
            else:
                return False
        except e:
            return False
