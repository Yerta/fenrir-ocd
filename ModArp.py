# coding=utf-8
######################################################################
##|# -------- modARP : ARP management module for MANGLE -------- #|###
##|# - It is responsible for handling ARP requests and replies - #|###
##|# - in order to avoid triggering switch's security measures - #|###
##|# - while still providing address resolution possibilities  - #|###
##|# -          to both the legitimate and rogue host          - #|###
######################################################################

from scapy.all import *
from scapy.layers.l2 import ARP, Ether

from FenrirTail import FenrirTail, fenrir_panic


###################################################################
### ---------------- Main component of modARP ---------------- ####
###################################################################
class ModArp:

    def __init__(self, ip_host, ip_rogue, mac_host, mac_rogue, debug_level=1):
        self.FenrirTail = FenrirTail(debug_level)
        self.debugLevel = debug_level
        self.FenrirTail.notify('Loading ARP module...', 1)
        self.ARPthreads = []
        self.ARPthread_number = 0
        self.host = ip_host
        self.rogue = ip_rogue
        self.mrogue = mac_rogue
        self.mhost = mac_host

    ## modARP main routine ##
    def fenrir_address_resolution_protocol(self, arp_pkt):
        if arp_pkt[ARP].op == 2:  # ARP-reply
            self.FenrirTail.notify('ARP reply received', 3)
            for arp_thread in self.ARPthreads:
                if arp_thread.this_is_my_arp(arp_pkt):
                    self.FenrirTail.notify('Corresponding ARPthread found', 3)
                    arp_thread.change_state()
                    self.FenrirTail.notify("ARPthread state changed from 'rep_sent' to 'rep_rcvd'", 3)
                    returned_pkt = self.arp_reply_mangling(arp_pkt, arp_thread)
                    self.delete_arp_thread(arp_thread)
                    return returned_pkt
            self.FenrirTail.notify('No corresponding ARPthread found', 3)
            return arp_pkt  # if no ARPthread, then forward the ARP-reply
        else:  # ARP request
            if not self.check_for_strange_arp(arp_pkt):  # check if ARP packet might compromise covertOps
                if arp_pkt[ARP].psrc == self.rogue or arp_pkt[ARP].psrc == self.host:
                    self.FenrirTail.notify('Request from rogue or host, mangling...', 3)
                    new_ARPthread = self.create_arp_thread(arp_pkt)
                    return self.arp_request_mangling(arp_pkt)
                elif arp_pkt[ARP].pdst == self.host:
                    self.FenrirTail.notify('Request for host, forwarding...', 3)
                    new_ARPthread = self.create_arp_thread(arp_pkt)
                    return arp_pkt  # if request for host, then forward
                else:
                    return False  # drop pkt
            else:
                return False  # drop pkt

    ## returns True if packet is strange and may need further processing, False otherwise ##
    def check_for_strange_arp(self, pkt):
        if pkt[Ether].dst == self.mrogue or pkt[ARP].pdst == self.rogue:
            self.FenrirTail.notify(
                'Strange ARP packet detected. Dropping it... You may want to check where this one came from : ', 1)
            ls(pkt)
            return True
        else:
            self.FenrirTail.notify('ARP request received', 3)
            return False

    ## Creates an ARPthread from an ARP-request packet ##
    def create_arp_thread(self, pkt):
        self.ARPthread_number += 1
        arp_thread_instance = ARPthread(self.debugLevel, pkt[Ether].src, pkt[Ether].dst, pkt[ARP].psrc, pkt[ARP].pdst,
                                        'req_sent')
        self.ARPthreads.append(arp_thread_instance)
        self.FenrirTail.notify('New ARPthread created', 3)
        return arp_thread_instance

    ## Deletion of complete ICMPthread ##
    def delete_arp_thread(self, arp_thread):
        try:
            if arp_thread.state == 'zombie':
                fenrir_panic(
                    'Unexpected situation occured during deletion of ARPthread (ARPthread is a zombie)')
            else:
                self.ARPthreads.remove(arp_thread)
                self.ARPthread_number -= 1
                self.FenrirTail.notify('ARPthread deleted', 3)
            return True
        except ValueError:
            fenrir_panic('Unexpected exception was raised during deletion of ARPthread')

    ## ARP Mangling Routines ##
    def arp_request_mangling(self, pkt):
        if pkt[ARP].psrc == self.rogue:
            return self.pkt_rewriter(pkt, self.host, 0, self.mhost, 0, self.mhost, 0)
        else:  # the ARP reply is for legit host
            return pkt

    def arp_reply_mangling(self, pkt, arp_thread):
        if arp_thread.src_mac == self.mrogue:  # the ARP reply is for rogue
            return self.pkt_rewriter(pkt, 0, self.rogue, 0, self.mrogue, 0, self.mrogue)
        else:  # the ARP reply is for legit host
            return pkt

    ## Rewrites ARP packets ##
    def pkt_rewriter(self, pkt, src, dst, msrc, mdst, hwsrc, hwdst):
        self.FenrirTail.notify('ARP packet is being rewritten :', 3)
        if src != 0:
            self.FenrirTail.notify('\t' + pkt[ARP].psrc + ' --> ' + src, 3)
            pkt[ARP].psrc = src
        if dst != 0:
            self.FenrirTail.notify('\t' + pkt[ARP].pdst + ' --> ' + dst, 3)
            pkt[ARP].pdst = dst
        if msrc != 0:
            pkt[Ether].src = msrc
        if hwsrc != 0:
            self.FenrirTail.notify('\t' + pkt[ARP].hwsrc + ' --> ' + hwsrc, 3)
            pkt[ARP].hwsrc = hwsrc
        if mdst != 0:
            pkt[Ether].dst = mdst
        if hwdst != 0:
            self.FenrirTail.notify('\t' + pkt[ARP].hwdst + ' --> ' + hwdst, 3)
            pkt[ARP].hwdst = hwdst
        pkt = pkt.__class__(str(pkt))
        self.FenrirTail.notify('ARP packet mangled and rewritten successfully', 3)
        return pkt


###################################################################
### --- Class representing an ARP exchange between 2 hosts --- ####
###################################################################
class ARPthread:
    states = ['req_sent', 'rep_rcvd', 'zombie']

    # SOURCE is always from the host point of view (spoofed/rogue host)
    def __init__(self, debug_level, msrc, mdst, asrc, adst, state="req_sent"):
        self.FenrirTail = FenrirTail(debug_level)
        self.src_mac = msrc
        self.src_ip = asrc
        self.dst_mac = mdst
        self.dst_ip = adst
        self.state = state

    def change_state(self):
        if self.state == 'req_sent':
            self.state = 'rep_rcvd'
        elif self.state == 'rep_rcvd':
            self.state = 'zombie'
        else:
            fenrir_panic(
                "STRANGE ARP STATE DETECTED : '" + self.state + "' - Look for the 'changeState' function in modARP.py")

    ## Returns True if a packet is a reply to a previous request
    def this_is_my_arp(self, pkt):
        if pkt[ARP].psrc == self.dst_ip:
            return True
        else:
            return False

    ## UTILS FUNCTIONS ##
    def logdump(self):
        print('mac src : ' + self.src_mac)
        print('ip src : ' + self.src_ip)
        print('mac dst : ' + self.dst_mac)
        print('ip dst : ' + self.dst_ip)
        print('state : ' + self.state)
